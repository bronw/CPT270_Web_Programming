<?php session_start(); ?>




<!DOCTYPE html>
<html lang="en">
<head>
    <title>printTicket.php</title>
    <?php require_once ('resources/fragments/genericHeader.php') ?>

</head>
<body>
<div class ="jumbotron">
    <h1>Silverardo</h1>
    <p>

    </p>
</div>


<!-- header -->
<?php require('resources/fragments/navBarV2.php') ?>




<div class="container">
    <div class="col-sm-6 col-md-5">
        <div class="panel">
            <form role="form" target="printTickets.php" method ="GET">
                <div class="form-group">
                    <label class="sr-only" for="name">Email address</label>
                    <input type="text" class="form-control" name="clientName" placeholder="Enter name">
                </div>

                <div class="form-group">
                    <label class="sr-only" for="email">Email address</label>
                    <input type="email" class="form-control" name="clientEmail" placeholder="Enter email">
                </div>

                <div class="form-group">
                    <label class="sr-only" for="phone">Phone</label>
                    <input type="text" pattern="#" class="form-control" name="clientPhone" placeholder="phone">
                </div>

                <button type="submit" class="btn btn-primary">Record </button>
            </form>
        </div>
    </div>
</div>




</body>
</html>