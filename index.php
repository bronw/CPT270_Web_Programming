<?php session_start();  ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>New</title>
    <?php require_once ('resources/fragments/genericHeader.php') ?>

    <style>
        p{
            font-size: 1.5em;
        }
        #final{
            font-size: 2em;
            font-weight: 400;
        }

    </style>


</head>
<body>

<div class ="jumbotron">
    <h1>Silverardo</h1>
    <p>
        We are back
    </p>
</div>


<!-- header -->
    <?php require('resources/fragments/navBarV2.php') ?>




<div class="container">

    <h1>So... Silverardo opens again </h1>

    <p>You know us ...we know you.</p>
    <p>We are the wise guys on the corner of Mains and Fourth Streets. We have new partners, the
        Corleone Family. <strong> Have your heard of them? </strong></p>

</div>

<!-- carousel -->
<?php require('resources/fragments/carouselV1.php') ?>

<div class="container">
    <br><br>
    <p>The Corleone's is getting out of the olive oil business, settling out here, in the
        west, western Sydney - go figure. </p>
    <p>You see, Don Michael Corleone says we gotta make the place nice.
        So we've been busy, wrecking the joint. Now it is nice, real nice. </p>

    <p> We have it all now. Here, in our town.
        Where you can park right out front.
    </p>
    <p>
        You know... being in the same town,
        it is important. It is more important then government,
        more important then sharing the same pub.
        It is almost as important as <span = class = "theFamily"> Family </span>.</p>
</div>

</body>
</html>
