<?php session_start();
require_once("resources/php_libraries/phpFunctions.php");
require_once("resources/php_libraries/buildOrderReservationsPage.php");
require_once("resources/php_libraries/manageReservations.php");



/******************************************************************
 *  FOR DEALING WITH THE CLIENT INFORMATION
 * *****************************************************************/

$clientInfoSet = true;

if(isset($_GET["clientName"])){

    $_SESSION["clientName"] = $_GET["clientName"];
    $_SESSION["clientEmail"] = $_GET["clientEmail"];
    $_SESSION["clientPhone"] = $_GET["clientPhone"];
    $clientInfoSet= true;
}


/******************************************************************
 *  client is making a reservation
 * *****************************************************************/
if(isset($_GET["makeReservation"])){

    // case: not client Info
    if (!(isset($_SESSION["clientName"]))){
        $clientInfoSet = false;
    }
    else{

    // record the movie ID so that
        // the print page can load it
        $reserveMovieID = $_GET["makeReservation"];
        $_SESSION["printingReservation"] =$reserveMovieID;

    //  goto the print page
        header("Location:printTickets.php");
    }
}



/******************************************************************
 *  client is trashing a reservation
 * *****************************************************************/
    if(isset($_GET["dropReservation"])){

        $reserveMovieID = $_GET["dropReservation"];
        unset($_SESSION['dropReservation']);
        removeDraftReservation($reserveMovieID);

    }


?>



<!DOCTYPE html>
<html lang="en">
<head>
    <title>orderReservation.php</title>
    <?php require_once ('resources/fragments/genericHeader.php') ?>
<link rel="stylesheet" type="text/css"
      href="resources/styles/ticketSelect.css"/>


</head>
<body>

<!--<div class = "container-fluid">

    <h2><?php /* echo "TELLING ME WHATS DOING RESERVE<br>";*/?></h2>

    <h4>
        <?php /* echo "here<br>";*/?>


    </h4>
</div>-->


<!--heading-->
<div class ="jumbotron">
    <h1>Reserve Your Bookings</h1>
    <p>
        Please confirm, then get your tickets
    </p>
</div>
<?php require_once('resources/fragments/navBarV2.php') ?>





<?php
    // on build order reservations page
    buildBookingContainers($clientInfoSet);
?>

<!--selected Ticket Items-->



