<!DOCTYPE html>
<html lang="en">
<head>
    <title>Silverardo</title>
    <?php require_once ('resources/fragments/genericHeader.php') ?>
    <link rel = "stylesheet"
          href="resources/styles/contactStyle.css">

</head>
<body>

<div class ="jumbotron">
    <h1>Contact Us</h1>
    <p>
        what do you have to say?
    </p>
</div>

<!-- header -->
<?php require('resources/fragments/navBarV2.php') ?>

<div class="container">

    <!-- form  -->
    <form class = "form-inline" role="form" id = contactForm
          action="http://titan.csit.rmit.edu.au/~e54061/wp/testcontact.php"
          method="post">
        <fieldset>
            <legend>Choose a subject</legend>

            <ol>
                <li>
                    <ul>
                        <li>
                            <div class="form-group">
                                <input
                                class = "form-control"
                                id=generalEnquiry
                                name=subject
                                type = radio
                                required>
                            <!--checked>-->
                                <label for=generalEnquiry>General Enquiry</label>
                            </div>

                        </li>
                        <li>
                            <div class="form-group">
                                <input
                                    class = "form-control"
                                    id=groupBookings
                                    name=subject
                                    type = radio
                                    required>
                                <label for=groupBookings>Group and Corporate Bookings</label>
                            </div>
                        </li>
                        <li>
                            <div class="form-group">
                                <input
                                    class = "form-control"
                                    id=suggestions
                                    name=subject
                                    type = radio
                                    required>
                                <label for=suggestions>Suggestions & Complaints</label>
                            </div>
                        </li>
                    </ul>
                </li>
            </ol>
        </fieldset>
        <fieldset>
            <legend>Enter your contact details and message</legend>
            <ol>

                <li>
                    <div class="form-group">
                        <label for = email>Email</label>
                        <input
                            class = "form-control"
                            id=email
                            name = email
                            type = email
                            placeholder = "youHandle@SomeShonkyWebsite.someWhere"
                            required
                            autofocus>
                    </div>

                </li>
                <li>

                    <div class="form-group">
                        <label for=message>Message</label>
                        <textarea
                            class = "form-control"
                            id =message
                            name = message
                            rows = 5
                            wrap = hard
                            placeholder="enter your comments here"
                            required>
                        </textarea>
                    </div>

                </li>

            </ol>
        </fieldset>

        <fieldset>
            <legend>Lodge it</legend>
            <button type=submit>Send!</button>
        </fieldset>
    </form>

</div>
</body>
</html>
