<?php session_start();

    require_once("resources/php_libraries/phpFunctions.php");

     if (isset($_GET["dropMovie"])){
         // drop the last movie record
         clearCurrentMovie();

                /*    echo "count of movieIDs after function: " .
                            count($_SESSION["cartMovies"]). "<br>";*/
     }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Now Showing</title>
    <?php require_once ('resources/fragments/genericHeader.php') ?>

    <link rel = "stylesheet"
          href="resources/styles/modalMoviePopups.css">

    <link rel = "stylesheet"
          href="resources/styles/showingTable.css">

    <script
        type="text/javascript"
        src="resources/scripts/selectMovieSession.js">
    </script>

</head>
<body>

<!--page header-->
<div class ="jumbotron">
    <h1>Now Showing</h1>
    <p>
        Pick a Movie
    </p>
</div>
<?php require('resources/fragments/navBarV2.php') ?>



<!--title-->
<div class="container">
    <div class ="row">
        <h2>Now on at the Silverado</h2>
    </div>
</div>


<!-- movie display 1-->
<div class="container">

        <div class="col-sm-6 col-md-5">
            <?php require('resources/fragments/movieDisplay_Kamasutra3D.php') ?>
        </div>

        <div class="col-sm-6 col-md-7">
            <?php require('resources/fragments/movieDisplay_Brave.php') ?>
        </div>

        <div class="col-sm-6 col-md-5">
            <?php require('resources/fragments/movieDisplay_LoveActually.php') ?>
        </div>

        <div class="col-sm-6 col-md-5">
            <?php require('resources/fragments/movieDisplay_GoldFinger.php') ?>
        </div>

</div>






</body>
</html>
