<?php session_start();  ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>New</title>
    <?php require_once ('resources/fragments/genericHeader.php') ?>



<style>
    p{
        font-size: 1.5em;
    }
    #final{
        font-size: 2em;
        font-weight: 400;
    }

</style>


</head>
<body>

<!--heading-->
<div class ="jumbotron">
    <h1>News</h1>
    <p>
        What's up with the family
    </p>
</div>
<?php require('resources/fragments/navBarV2.php') ?>


<div class="container-fluid col-md-6 col-sm-12">



        <H1>Time to Cut the Rug </H1>
        <figure id = "whatsNew1">
            <img
                src="resources/img/Don_whisperItToMe.jpg"
                class = "img-responsive img-rounded"
                alt="did you say that">
        </figure>


        <p>The place is real flash now. <br>
            Our re-opening is a party and nothing but the best for
            our guests. Who knows... maybe Johnny Fontane might show up and sing
            a few numbers. </p>

</div>
<div class="container-fluid col-md-6 col-sm-12">
    <h2>Great Chairs</h2>
        <p>We have installed new seats.</p>


        <figure  id = "whatsNew4">
            <img
                src="resources/img/Fredo1.jpg"
                class = "img-responsive img-circle"
                alt="TheFro">
        </figure>

        <p>We have exclusive gold class seats - just like Michael's favourite.
            </p>

        <p> And there is nothing standard about our standard. They are good
            enough for your best friend (or your worst enemy).
        </p>
        <p> And we have bean-bags! <br>
            Which you can share - Fredo specially likes those.
        </p>

</div>
<div class="container-fluid col-md-6 col-sm-12">
    <h2 class ="clearItLeft">Eye popping Projectors</h2>
        <p>It doesn't stop there! we have also installed a sophisticated
            3D <del>surveillance</del> projection system.
        </p>
        <p>The images of Sonny stamping his ticket at the Candy Island toll booth
            with stay with you for the rest of your life.
        </p>

        <figure id = "whatsNew2">
            <img
                src="resources/img/candybar_Godfather_Luca_Brasi_by_donvito62.jpg"
                class = "img-responsive img-rounded"
                alt="sleepsWithFish">
        </figure>
</div>
<div class="container-fluid col-md-6 col-sm-12">
    <h2>Bigger Sound</h2>
        <p>We have you surrounded - with sound!
        </p>

        <p>Dolby Cinema Surround Sound! Wow! Cutting heads off horses
            has never sounded so good,
            <a href="http://www.dolby.com/us/en/cinema/index.html">
                check it out! </a>.
        </p>

        <figure  id = "whatsNew3">
            <img
                src="resources/img/deadHorse_article-godfather2-0405.JPG"
                class = "img-responsive img-rounded"
                alt="horseRacing">
        </figure>

</div>

<div class="container-fluid col-md-12 col-sm-12">
        <p id = "final"> Because, as Don Vito Corleone says, a man that doesn't spend time with his
            family - at the movies,

            <span = class = "theFamily">  he can never be a real man. </span>
        </p>



</div>
</body>
</html>