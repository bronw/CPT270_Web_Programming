# Notes

This assignment was in two parts: 
1. assignment 1 - Build the web site in basic html

2. assignment 2 - Restructure the web site with great usage of PHP.

The repository contains the submitted version for assignment 2, however, both versions can be viewed at:

  http://saturn.csit.rmit.edu.au/~s3460996/wp/a1/index.php

  http://saturn.csit.rmit.edu.au/~s3460996/wp/a2/index.php
