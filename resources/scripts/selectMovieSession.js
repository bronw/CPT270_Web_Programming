/**
 * Created by Anthony on 13/02/2016.
 */

function gotoBookingPage(sourceID, event){


// ensure table data row
    if(!(event.target.nodeName=="TD")){
        return;
    }

//  get row data and put it into session variable
    var TRcells = event.target.parentNode.cells;
    var sessionDay = TRcells[0].innerHTML;
    var sessionTime = TRcells[1].innerHTML;
    var priceGroup= TRcells[2].innerHTML;


    sessionStorage.setItem('movieGenre',sourceID);
    sessionStorage.setItem('sessionTime',sessionTime);
    sessionStorage.setItem('sessionDay',sessionDay);
    sessionStorage.setItem('priceGroup',priceGroup);

//  fill the form cells
    var formDay = document.getElementById(sourceID+"IDday");
    var formTime = document.getElementById(sourceID+"IDtime");

    formDay.value = sessionDay;
    formTime.value = sessionTime;



//  confirm selection before going to cart
    displayAlert(sourceID,sessionTime,sessionDay);

}

function displayAlert(sourceID,sessionTime,sessionDay){
//  confirm selection with an alert


    var alertText= document.getElementById(sourceID+"alertText");
    alertText.innerHTML = "See this movie on " + sessionDay +
                            " at " + sessionTime + "?";

    changeTableVisibility(sourceID,false);
    changeAlertVisibility(sourceID,true);

}

function changeTableVisibility (sourceID,newState){
// toggle table
    var alertDiv = document.getElementById(
        sourceID +"BookingSelector");

    if (newState){
        //alertDiv.style.visibility = "visible";
        alertDiv.style.display = "";

    }else{
        alertDiv.style.display = "none";
    }

}

function changeAlertVisibility (sourceID,newState){
// toggle alert
    var alertDiv = document.getElementById(sourceID+"confirmBooking");

    if (newState){
        //alertDiv.style.visibility = "visible";
        alertDiv.style.display = "block";
    }else{
        alertDiv.style.display = "none";
    }

}


function alertClose(sourceID){
// hide alert and bring table back
    changeAlertVisibility(sourceID,false);
    changeTableVisibility(sourceID,true);
}






// trialing HTML5 session variable
function runStuff(){

    sessionStorage.clear();
    setSessionInfo();
    checkForSessionInformation();

}

function checkForSessionInformation(){

    var movieGenre = sessionStorage.getItem('movieGenre');
    var sessionTime = sessionStorage.getItem('sessionTime');
    var sessionDay = sessionStorage.getItem('sessionDay');
    var priceGroup = sessionStorage.getItem('priceGroup');

    document.getElementById("movieGenre").innerHTML = movieGenre;
    document.getElementById("sessionTime").innerHTML= sessionTime;
    document.getElementById("sessionDay").innerHTML = sessionDay;

}

function setSessionInfo(){


    sessionStorage.setItem('sessionTime','sessionTimeEntry');
    sessionStorage.setItem('sessionDay','sessionTimeRecord');
}

