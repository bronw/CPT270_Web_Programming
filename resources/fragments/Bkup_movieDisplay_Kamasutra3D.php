


<div class="panel panel-default">

    <div class ="panel-heading">
        <h3>Kamasutra-3D</h3>
    </div>
    <div class ="panel-body">
        <div id = "Pic">

            <a href="#"class="thumbnail">
                <img src="resources/img/Kamasutra-3D-poster.jpg"
                     class="img-thumbnail"
                     alt="Kamasutra-3D Poster"
                     data-toggle="modal"
                     data-target="#myModal"
                >
            </a>
        </div>

        <div >
            <h4>Summary</h4>
            <p>Love is like war - everyone dies</p>
            <h4>Rating</h4>
            <p>Definitely don't bring the kids</p>
            <h4>Plot</h4>
            <p>Doesn't matter how good you move, <br>
               someone will always out fox-trot you</p>


            <button type="button"
                    class="btn btn-info btn-lg"
                    data-toggle="modal"
                    data-target="#myModal">
                Read More
            </button>

        </div>

        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button
                            type="button"
                            class="close"
                            data-dismiss="modal">&times;
                        </button>
                        <h4 class="modal-title">Kamasutra-3D</h4>
                    </div>
                    <div class="modal-body">
                        <iframe
                            class = "center-block"
                            src="https://www.youtube.com/embed/hLvbdyC8T24?rel=0"
                            frameborder="0"
                            allowfullscreen>
                        </iframe>
                    </div>

                    <!-- table -->
                    <?php require 'showingTable_Foreign.php'; ?>

                    <div class="modal-footer">
                        <button
                            type="button"
                            class="btn btn-default"
                            data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


