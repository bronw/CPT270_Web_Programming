



<div class="container">
    <div class="col-sm-6 col-md-5">
        <div class="panel panel-default">
            <form role="form">
                <div class="form-group">
                    <label class="sr-only" for="name">Email address</label>
                    <input type="text" class="form-control" id="name" placeholder="Enter name">
                </div>

                <div class="form-group">
                    <label class="sr-only" for="email">Email address</label>
                    <input type="email" class="form-control" id="email" placeholder="Enter email">
                </div>

                <div class="form-group">
                    <label class="sr-only" for="phone">Phone</label>
                    <input type="text" pattern="#" class="form-control" id=phone" placeholder="phone">
                </div>

                <button type="submit" class="btn btn-primary">Record </button>
            </form>
        </div>
    </div>
</div>