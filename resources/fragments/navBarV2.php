
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">Silverado</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">

                <li> <a href="index.php">
                        <span class="glyphicon glyphicon-home"></span>
                        </a>
                </li>
                <li><a href="whatsNew.php">
                        <span class="glyphicon glyphicon-bullhorn"></span>
                        News</a></li>
                <li> <a href="TimesAndPrices.php">
                        <span class="glyphicon glyphicon-time"></span>
                        Schedules</a> </li>
                <li> <a href=nowShowing.php>
                        <span class="glyphicon glyphicon-film"></span>
                        Showing</a> </li>
                <li><a href=contactForm.php>
                        <span class="glyphicon glyphicon-envelope">
                        </span>
                        Contact us
                    </a>
                </li>
            </ul>


            <ul class="nav navbar-nav navbar-right">
                <li> <a href=orderReservation.php>
                        <span class="glyphicon glyphicon-shopping-cart"></span>
                        Shopping
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>

