


<div class="panel panel-default">

    <div class="panel-heading">
        <h3>Brave</h3>
    </div>
    <div class ="panel-body">
        <div>

            <a href="#"class="thumbnail">
                <img src="resources/img/brave-620x266.png"
                     class="img-thumbnail"
                     alt="brave girl"
                     data-toggle="modal"
                     data-target="#ModalBrave"
                >
            </a>
        </div>

        <div>
            <h4>Summary</h4>
            <p> Pixar's Fantastic Feminist Documentary</p>
            <h4>Rating</h4>
            <p>for the kids</p>
            <h4>Plot</h4>
            <p>
                What can I say about this movie? Not much
                because I went and mowed the lawn while it
                was on… kids liked it but.
            </p>


            <button type="button"
                    class="btn btn-info btn-lg"
                    data-toggle="modal"
                    data-target="#ModalBrave"
                    onclick ="alertClose('kids')" >
                Read More
            </button>

        </div>

        <div class="modal fade" id="ModalBrave" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button
                            type="button"
                            class="close"
                            data-dismiss="modal">&times;
                        </button>
                        <h4 class="modal-title">Brave</h4>
                    </div>
                    <div class="modal-body">

                        <iframe
                            class = "center-block"
                            src="https://www.youtube.com/embed/TEHWDA_6e3M?rel=0"
                            frameborder="0"
                            allowfullscreen>
                        </iframe>

                        <!-- table -->
                        <?php require 'showingTable_Kids.php'; ?>

                    </div>
                    <div class="modal-footer">
                        <button
                            type="button"
                            class="btn btn-default"
                            data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


