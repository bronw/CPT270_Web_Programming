



<!--    ROMCOM-->
<div id="romcom">
    <table id = "romcomBookingSelector"
           class="table table-hover table-condensed displayTimeDayTable"
           onclick="gotoBookingPage('romcom',event)">
        <thead>
        <tr>
            <th>Day</th>
            <th>Time</th>
            <th>Price</th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td>Monday</td>
            <td>9pm</td>
            <td>budget</td>
        </tr>
        <tr>
            <td>Tuesday</td>
            <td>9pm</td>
            <td>budget</td>
        </tr>
        <tr>
            <td>Wednesday</td>
            <td>1pm</td>
            <td>budget</td>
        </tr>
        <tr>
            <td>Thursday</td>
            <td>1pm</td>
            <td>budget</td>
        </tr>
        <tr>
            <td>Friday</td>
            <td>1pm</td>
            <td>std</td>
        </tr>
        <tr>
            <td>Saturday</td>
            <td>6pm</td>
            <td>std</td>
        </tr>
        <tr>
            <td>Sunday</td>
            <td>6pm</td>
            <td>std</td>
        </tr>
        </tbody>
    </table>

    <div id = "romcomconfirmBooking"
         style ="display: none"
         class="alert alert-success fade in">

        <a
            id="close"
            aria-label="close"
            onclick = "alertClose('romcom')"
            style="float:right">
            <span class="glyphicon glyphicon-remove"></span>
        </a>

        <form id= "MovieSessionInfo"
              action="ticketSelection.php"
              method="get">
            <input type = hidden id="romcomIDday" name="day" value="Funday" />
            <input type = hidden id="romcomIDtime" name="time" value="1am" />
            <button type="submit"
                    value="Make Booking"
                    class="alert alert-success btn-block"
                    id = "bookBtn">

                <h3><strong>Make Booking?</strong></h3>
                <h4 id = "romcomalertText">
                    Display time and day.
                </h4>
            </button>
        </form>

    </div>



</div>