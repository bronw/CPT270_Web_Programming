

<!--style="border: none"-->





<!--    Kids-->
<div id="kids" class = "well tablesInModal">
    <table id = "kidsBookingSelector"
           class="table table-hover table-condensed displayTimeDayTable"
           onclick="gotoBookingPage('kids',event)">
        <thead>
        <tr>
            <th>Day</th>
            <th>Time</th>
            <th>Price</th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td>Monday</td>
            <td>1pm</td>
            <td>budget</td>
        </tr>
        <tr>
            <td>Tuesday</td>
            <td>1pm</td>
            <td>budget</td>
        </tr>
        <tr>
            <td>Wednesday</td>
            <td>6pm</td>
            <td>std</td>
        </tr>
        <tr>
            <td>Thursday</td>
            <td>6pm</td>
            <td>std</td>
        </tr>
        <tr>
            <td>Friday</td>
            <td>6pm</td>
            <td>std</td>
        </tr>
        <tr>
            <td>Saturday</td>
            <td>12pm</td>
            <td>std</td>
        </tr>
        <tr>
            <td>Sunday</td>
            <td>12pm</td>
            <td>std</td>
        </tr>
        </tbody>
    </table>


    <div id = "kidsconfirmBooking"
         style ="display: none"
         class="alert alert-success fade in">

        <a
            id="kidsclose"
            aria-label="close"
            onclick = "alertClose('kids')"
            style="float:right">
            <span class="glyphicon glyphicon-remove"></span>
        </a>

        <form id= "kidsMovieSessionInfo"
              action="ticketSelection.php"
              method="get">
                <input type = hidden id="kidsIDday" name="day" value="Funday" />
                <input type = hidden id="kidsIDtime" name="time" value="1am" />
                <button type="submit"
                        value="Make Booking"
                        class="alert alert-success btn-block"
                        id = "kidsbookBtn">

                    <h3><strong>Make Booking?</strong></h3>
                    <h4 id = "kidsalertText">
                        Display time and day.
                    </h4>
                </button>
        </form>

    </div>


</div>

