
<div  class="panel panel-default">
    <div class="panel-heading">
        <h1>Session Genre Times</h1>
    </div>
    <div class="panel-body">

        <div>
            <table id ="screenTable" class="table table-hover table-condensed">

                <tr>
                    <th class="blankCell"></th>
                    <th colspan="2">midday</th>
                    <th >afternoon</th>
                    <th >evening</th>
                    <th>night</th>
                </tr>

                <tr>
                    <th class="blankCell"></th>
                    <th >12:00 PM</th>
                    <th >1:00 PM</th>
                    <th >3:00 PM</th>
                    <th >6:00 PM</th>
                    <th>9:00 PM</th>
                </tr>

                <tr>
                    <th>Monday</th>
                    <td class="blankCell"></td>
                    <td rowspan="2" id = "kid">Childerns</td>
                    <td class="blankCell"></td>
                    <td rowspan="2" id = "for"> Art/Foreign</td>
                    <td rowspan="2" id = "romcom">Romantic Comedy</td>
                </tr>

                <tr>
                    <th>Tuesday</th>
                </tr>

                <tr>
                    <th>Wednesday</th>
                    <td class="blankCell"></td>
                    <td rowspan="3" id = "romcom">Romantic Comedy</td>
                    <td class="blankCell"></td>
                    <td rowspan="3" id = "kid">Childerns</td>
                    <td rowspan="3" id = "act">Action</td>
                </tr>

                <tr>
                    <th>Thursday</th>
                </tr>

                <tr>
                    <th>Friday</th>
                </tr>

                <tr>
                    <th>Saturday</th>
                    <td  rowspan="2" id = "kid">Childerns</td>
                    <td class="blankCell"></td>
                    <td  rowspan="2" id = "for"> Art/Foreign</td>
                    <td  rowspan="2" id = "romcom">Romantic Comedy</td>
                    <td rowspan="2" id = "act">Action</td>
                </tr>

                <tr>
                    <th>Sunday</th>
                </tr>
            </table>
        </div>
    </div>
</div>