



<!-- ACTION-->
<div id="action" class = "well tablesInModal">
    <table id = "actionBookingSelector"
           class="table table-hover table-condensed displayTimeDayTable"
           onclick="gotoBookingPage('action',event)">
        <thead>
        <tr>
            <th>Day</th>
            <th>Time</th>
            <th>Price</th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td>Wednesday</td>
            <td>9pm</td>
            <td>std</td>
        </tr>
        <tr>
            <td>Thursday</td>
            <td>9pm</td>
            <td>std</td>
        </tr>
        <tr>
            <td>Friday</td>
            <td>9pm</td>
            <td>std</td>
        </tr>
        <tr>
            <td>Saturday</td>
            <td>9pm</td>
            <td>std</td>
        </tr>
        <tr>
            <td>Sunday</td>
            <td>9pm</td>
            <td>std</td>
        </tr>
        </tbody>
    </table>


    <div class="alert alert-success fade in"
         id = "actionconfirmBooking"
         style ="display: none"
         class="alert alert-success fade in">

        <a
            id="actionclose"
            aria-label="close"
            onclick = "alertClose('action')"
            style="float:right">
            <span class="glyphicon glyphicon-remove"></span>
        </a>

        <form id= "actionMovieSessionInfo"
              action="ticketSelection.php"
              method="get">
            <input type = hidden id="actionIDday" name="day" value="Funday" />
            <input type = hidden id="actionIDtime" name="time" value="1am" />
            <button type="submit"
                    value="Make Booking"
                    class="alert alert-success btn-block"
                    id = "actionbookBtn">

                <h3><strong>Make Booking?</strong></h3>
                <h4 id = "actionalertText">
                    Display time and day.
                </h4>
            </button>
        </form>

    </div>
</div>