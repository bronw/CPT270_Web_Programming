

<div class="panel panel-default">

    <div class ="panel-heading">
        <h3>Love Actually</h3>
    </div>
    <div class ="panel-body">
        <div>

            <a href="#"class="thumbnail">
                <img src="resources/img/loveActually2.jpg"
                     class="img-thumbnail"
                     alt="Looking like a chump"
                     data-toggle="modal"
                     data-target="#ModalLoveActually"
                >
            </a>
        </div>

        <div>
            <h4>Summary</h4>
            <p> Just a guy, standing in front of a girl,
                asking her to 'pull my finger'. </p>
            <h4>Rating</h4>
            <p>why waste your extras cover on a lobotomy?</p>
            <h4>Plot</h4>
            <p>After which the Silverado's extra-ply <i>‘tissues
                    actually’ </i> tissue-box was named, this movie
                is a vague conglomerate of unrelated stories
                played-out across the city of London. Each
                descending inexorably towards their gruesome,
                but heart-warming end.<br><br>

                Recommended to all ages, but if you are with
                your granny… get two boxes. </p>


            <button type="button"
                    class="btn btn-info btn-lg"
                    data-toggle="modal"
                    data-target="#ModalLoveActually"
                    onclick ="alertClose('romcom')">
                Read More
            </button>

        </div>

        <div class="modal fade" id="ModalLoveActually" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button
                            type="button"
                            class="close"
                            data-dismiss="modal">&times;
                        </button>
                        <h4 class="modal-title">Love Actually</h4>
                    </div>
                    <div class="modal-body">
                        <iframe
                            class = "center-block"
                            src="https://www.youtube.com/embed/KdzH6a-XEGM?rel=0"
                            frameborder="0"
                            allowfullscreen>
                        </iframe>


                        <!-- table -->
                        <?php require 'showingTable_romCom.php'; ?>
                    </div>

                    <div class="modal-footer">
                        <button
                            type="button"
                            class="btn btn-default"
                            data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


