

<!--<!DOCTYPE html>-->
<!--<html lang="en">-->
<head>
<!--    <title>Bootstrap Example</title>-->
<!--    <meta charset="utf-8">-->
<!--    <meta name="viewport" content="width=device-width, initial-scale=1">-->
<!--    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">-->
<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>-->
<!--    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
   <style>


        .item img {

            height: 200px;
            max-height: 200px;
            margin: auto;
            width: auto;
        }

    </style>
</head>
<!--<body>-->


<div class="container">
    <br>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="resources/img/wisphers_02-godfather-promo.jpg"
                     alt="GodFather1" width="460" height="345">
                <div class="carousel-caption">
                    <p>
                        You can't say it? <br>
                        Come here, whisper it in my ear
                    </p>
                </div>
            </div>

            <div class="item">
                <img src="resources/img/MichaelInDadsChair.png"
                     alt="GodFather2" width="460" height="345">
                <div class="carousel-caption">
                    <p>
                        A man who doesn't take his famliy to the movies, well... <br>
                        He can't ever be a real man
                    </p>
                </div>
            </div>

            <div class="item">
                <img src="resources/img/deadHorse_article-godfather2-0405.JPG"
                     alt="GodFather3" width="460" height="345">
                <div class="carousel-caption">
                    <p>
                        Make him an offer he can't refuse
                    </p>
                </div>
            </div>

            <div class="item">
                <img src="resources/img/candybar_Godfather_Luca_Brasi_by_donvito62.jpg"
                     alt="GodFather4" width="460" height="345">
                <div class="carousel-caption">
                    <p>
                        It is an italian message. It means Luca Brasi <br>
                        sleeps with the fishs
                    </p>
                </div>

            </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

</div>


<!--
</body>
</html>
    --!>