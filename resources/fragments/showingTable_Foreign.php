



<!-- foreign-->
<div id="foreign" class = "well tablesInModal">
    <table id = "foreignBookingSelector"
           class="table table-hover table-condensed displayTimeDayTable"
           onclick="gotoBookingPage('foreign',event)">
        <thead>
        <tr>
            <th>Day</th>
            <th>Time</th>
            <th>Price</th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td>Monday</td>
            <td>6pm</td>
            <td>budget</td>
        </tr>
        <tr>
            <td>Tuesday</td>
            <td>6pm</td>
            <td>budget</td>
        </tr>
        <tr>
            <td>Saturday</td>
            <td>3pm</td>
            <td>std</td>
        </tr>
        <tr>
            <td>Sunday</td>
            <td>3pm</td>
            <td>std</td>
        </tr>
        </tbody>
    </table>


    <div id = "foreignconfirmBooking"
         style ="display: none"
         class="alert alert-success fade in">

        <a
            id="close"
            aria-label="close"
            onclick = "alertClose('foreign')"
            style="float:right">
            <span class="glyphicon glyphicon-remove"></span>
        </a>


        <form id= "foreignMovieSessionInfo"
              action="ticketSelection.php"
              method="get">
            <input type = hidden id="foreignIDday" name="day" value="Funday" />
            <input type = hidden id="foreignIDtime" name="time" value="1am" />
            <button type="submit"
                    value="Make Booking"
                    class="alert alert-success btn-block"
                    id = "foreignbookBtn">

                <h3><strong>Make Booking?</strong></h3>
                <h4 id = "foreignalertText">
                    Display time and day.
                </h4>
            </button>
        </form>


    </div>
</div>