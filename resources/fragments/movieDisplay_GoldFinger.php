

<div class="panel panel-default">

    <div class ="panel-heading">
        <h3>Gold finger</h3>
    </div>
    <div class ="panel-body">
        <div >

            <a href="#"class="thumbnail">
                <img src="resources/img/goldfinger_poster.jpg"
                     class="img-thumbnail"
                     alt="Looking like a chump"
                     data-toggle="modal"
                     data-target="#ModalGoldFinger"
                >
            </a>
        </div>

        <div >
            <h4>Summary</h4>
            <p>
                the origin of '<i> Laser beam </i>'
                <br>
                And of <i>My name is Pussy Galore</i>
            </p>
            <h4>Rating</h4>
            <p>Once 'bad-arse', now... not so much</p>
            <h4>Plot</h4>
            <p>
                Dr No died like no doctor should, but Bond returned.
                And painted the town red, and the women gold - and Ms
                Moneypenny? Well, any which way she liked.
            </p>


            <button type="button"
                    class="btn btn-info btn-lg"
                    data-toggle="modal"
                    data-target="#ModalGoldFinger"
                    onclick ="alertClose('action')" >
                Read More
            </button>

        </div>

        <div class="modal fade" id="ModalGoldFinger" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button
                            type="button"
                            class="close"
                            data-dismiss="modal">&times;
                        </button>
                        <h4 class="modal-title">Gold Finger</h4>
                    </div>
                    <div class="modal-body">
                        <iframe
                            class = "center-block"
                            src="https://www.youtube.com/embed/KdQoSK9wibU?rel=0"
                            frameborder="0"
                            allowfullscreen>
                        </iframe>

                        <!-- table -->
                        <?php require 'showingTable_action.php'; ?>

                    </div>
                    <div class="modal-footer">
                        <button
                            type="button"
                            class="btn btn-default"
                            data-dismiss="modal">Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


