<?php
/**
 * Created by PhpStorm.
 * User: Anthony
 * Date: 21/02/2016
 * Time: 9:15 PM
 */

//  client has decided to discard the reservation
    // need to remove, or change status.
function removeDraftReservation($reserveMovieID){

//  change the status of the movie Record

    setMovieIDStatus($reserveMovieID,"trashed");


}

//  client has decided to make the reservation
    // need to change status and print tickets
function completeTicketReservation($reserveMovieID){

//  get the movie array (and change its status whilst I'm at it)
    setMovieIDStatus($reserveMovieID,"booked");


}




// change the status of the movie by ID
function setMovieIDStatus($reserveMovieID,$newStatus){

    $number = 0;
    foreach($_SESSION['cartMovies'] as $movies)
    {
        if($movies["ID"]==$reserveMovieID){
            $_SESSION['cartMovies'][$number]['status'] = $newStatus;
        }
        $number++;
    }

}



// Used to put the SESSION variables back into the form
function clientText($inputName)
{

    $inputText = null;

    switch ($inputName) {
        case "clientName":
            if (isset($_SESSION["clientName"])) {
                $inputText = $_SESSION["clientName"];
            }
            break;
        case "clientPhone":
            if (isset($_SESSION["clientPhone"])) {
                $inputText = $_SESSION["clientPhone"];
            }
            break;
        case "clientEmail":
            if (isset($_SESSION["clientEmail"])) {
                $inputText = $_SESSION["clientEmail"];
            }
            break;
    }

    return $inputText;
}

