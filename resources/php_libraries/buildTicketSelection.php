<?php
/**
 * Created by PhpStorm.
 * User: Anthony
 * Date: 21/02/2016
 * Time: 12:47 PM
 */




//*****************************************************
//* make - display movie info
//******************************************************


function buildHeadingContainer($movie,$day,$time){

    $heading2 ="The Movie You Selected";

    $midComponent = buildHeadingMiddleComponent($movie,$day,$time,$heading2);


    $headingPart = "
            <!--show movie selection-->
        <div class=\"fluidContain container-fluid col-md-6 col-sm-12\">
        "
        .$midComponent.
        "
        </div>
        ";

    echo $headingPart;


}

// needed for the booking page
function buildHeadingMiddleComponent($movie,$day,$time,$heading2){


    $part = "
    <h2> $heading2 </h2>
    <div id=\"movieDisplay1\">
        <div class=\"movieDisplay\">
            <span ID=\"movieName\">".$movie."</span>
            <span ID=\"movieDay\">".$day."</span>
            <span ID=\"movieTime\">".$time."</span>
        </div>
    </div>
    ";
    return $part;
}





//*****************************************************
//* make ticket container
//******************************************************

function buildTicketContainer($PriceGroup){




    echo "<div class=\"fluidContain container-fluid col-sm-12 col-md-5\">";
    echo "<h2>Choose Tickets</h2>";
    echo "<div class=\"TicketContainer\">";

    buildTicketButtons($PriceGroup);

    echo "</div>";
    echo "</div>";

}


function buildTicketButtons($PriceGroup){


    //  button colors array
    $buttonColors = array();
    $buttonColors['Standard']= "btn-info";
    $buttonColors['Gold Class']= "btn-warning";
    $buttonColors['BeanBag']= "btn-success";
    $tempTickets = getTicketPriceList($PriceGroup);

    foreach ($tempTickets as $ticketArray) {

        //  choose button color
        $ticketType = $ticketArray["type"];
        $ticketColor = $buttonColors[$ticketType];


        //  make the button
        echo "<form style = \"display: inline-block\"
                    action=\"ticketSelection.php\" method=\"get\">";
        echo "<input type=\"hidden\"
                    name=\"ticketType\"
                    value=\"".
            $ticketArray["code"] ."\">";

        echo "<button type=\"submit\"
                        type=\"button\" class=\"btn " .
            $ticketColor . " TicketBtn Bt1\">";

        echo "<span>" . $ticketArray["type"] . "<br>".
            $ticketArray["subType"] . "  </span>";

        echo "<span class=\"badge\">" . $ticketArray["price"] .
            "</span>";

        echo "</button>";
        echo "</form>";
    }



}



//*****************************************************
//* make display selected tickets
//******************************************************

// handles the whole build
function buildDisplayTicketsSelected2(){



    $currentMovieArray = getCurrentMovieArray();
    $currentTicketsArray = $currentMovieArray["tickets"];
    $priceGroup = $currentMovieArray["priceGroup"];
    $countOfTickets = count($currentTicketsArray);



//  get ticket summary info to display
    $ticketSummaryResults = getTicketsSummary2($currentTicketsArray,$priceGroup);

//  get the parts out of the package
    $ticketList = $ticketSummaryResults["list"];
    $totalPrice = $ticketSummaryResults["totalPrice"];
    $totalNumber = $ticketSummaryResults["totalNumber"];
    $part2 = "";


    $part1 = "
    <div id = \"selectionContainer\"
         class=\"fluidContain container-fluid col-md-6 col-sm-12\">
        <h2>Selected Items</h2>
    <div class=\"itemsList\">
        <ul id = \"itemsToAdd\">
            <li class=\"row columnCaptions\">
                <span>QTY</span>
                <span>ITEM</span>
                <span>PRICE</span>
            </li>
            ";




    if($countOfTickets>0){
        $part2 = addSelectedTicketsRowItems2($ticketList);
    }

    $part3 = "

            <li class=\"row\" id=\"summed\">
                <span class=\"quantity\">".$totalNumber."</span>
                <span class=\"price\">".$totalPrice."</span>
            </li>
        </ul>
    </div>

    <div id = \"AlignReject\">
        <form  action=\"nowShowing.php\" method=\"get\">
        <input type=\"hidden\" name=\"dropMovie\" value=\"true\"/>
        <input
                type=\"submit\"
                value=\"GIVE IT UP\"
                id=\"reject\"
                class=\"finishOut btn btn-primary btn-lg\">
        </form>
    </div>

    <div id = \"AlignOrder\">
        <form action=\"orderReservation.php\" method=\"get\">
        <input type=\"hidden\" name=\"bookMovie\" value=\"true\"/>
        <input
                type=\"submit\"
                value=\"GO TO CART\"
                id=\"order\"
                class=\"finishGo btn btn-danger btn-lg\">
        </form>
    </div>
</div>
    ";

    echo $part1.$part2.$part3;

}

// makes the list bit in the middle
function addSelectedTicketsRowItems2($ticketList){

    $listPart = "";

    if (count($ticketList)>0){
        foreach($ticketList as $ticket){

            $listPart .="

                <li class=\"row \">
                    <span class=\"quantity\">".$ticket["count"]."</span>
                    <span class=\"TicketOrdered\">".$ticket["name"]."</span>
                    <span class=\"delBtn\"><a class=\"glyphicon glyphicon-remove\"></a></span>
                    <span class=\"price\">".$ticket["cost"]."</span>
                </li>
                ";
        }
    }
    return $listPart;
}




//*****************************************************
//* DUMPED
//******************************************************
function DONTUSE(){


/* function I amended

/*function buildDisplayTicketsSelected(){

    $currentMovieArray = getCurrentMovieArray();
    $currentTicketsArray = $currentMovieArray["tickets"];
    $countOfTickets = count($currentTicketsArray);
    $totalPrice = 0.0;
    $totalNumber = 0;

    echo "
    <div id = \"selectionContainer\"
         class=\"container-fluid col-md-6 col-sm-12\">
        <h2>Selected Items</h2>
    <div class=\"itemsList\">
        <ul id = \"itemsToAdd\">
            <li class=\"row columnCaptions\">
                <span>QTY</span>
                <span>ITEM</span>
                <span>PRICE</span>
            </li>
            ";


    if($countOfTickets>0){
        $ticketSummaryResults = addSelectedTicketsRowItems();
        $totalPrice = $ticketSummaryResults["totalPrice"];
        $totalNumber = $ticketSummaryResults["totalNumber"];
    }


    echo "

            <li class=\"row\" id=\"summed\">
                <span class=\"quantity\">".$totalNumber."</span>
                <span class=\"price\">".$totalPrice."</span>
            </li>
        </ul>
    </div>
    <button type=\"button\"
            type=\"reset\"
            id=\"reject\"
            class=\"finishOut btn btn-primary btn-lg\">
    GIVE IT UP
    </button>

    <button type=\"button\"
            type=\"submit\"
            id = \"order\"
            class=\"finishGo btn btn-danger btn-lg\">
    GO TO CART
    </button>

    </div>
    ";

}*/


/*function addSelectedTicketsRowItems(){


//  get info to display
    $ticketSummaryResults = getTicketsSummary();
    $ticketList = $ticketSummaryResults["list"];


    foreach($ticketList as $ticket){

        echo "

            <li class=\"row \">
                <span class=\"quantity\">".$ticket["count"]."</span>
                <span class=\"TicketOrdered\">".$ticket["name"]."</span>
                <span class=\"delBtn\"><a class=\"glyphicon glyphicon-remove\"></a></span>
                <span class=\"price\">".$ticket["cost"]."</span>
            </li>


            ";
    }

    return $ticketSummaryResults;
}*/
/*
// get array to support the list build
    function getTicketsSummary(){
        $currentMovieArray = getCurrentMovieArray();
        $currentTicketsArray = $currentMovieArray["tickets"];
        $totalPrice = 0.0;
        $totalNumber = 0;
        $ticketQuery = array();
        $tempTicketSum = array();
        $matched =false;

//  get an array of ticket types from data
        $ticketList = getTicketPriceList($currentMovieArray["priceGroup"]);

        if(!(count($currentTicketsArray)>0)){
            return null;
        }


        foreach($ticketList as $ticketDATA){

            $ticketQuery["code"]=$ticketDATA["code"];
            $ticketQuery["name"]=$ticketDATA["type"]."-".$ticketDATA["subType"];
            $ticketQuery["cost"]=0.0;
            $ticketQuery["count"]=0;

            foreach($currentTicketsArray as $ticket){

                if($ticketQuery["code"]==$ticket){
                    $matched = true;
                    $ticketQuery["cost"]+= dollarToInt($ticketDATA["price"]);
                    $ticketQuery["count"]++;
                }
            }

            if ($matched){
                $totalPrice += $ticketQuery["cost"];
                $totalNumber += $ticketQuery["count"];
                array_push($tempTicketSum,$ticketQuery);
                $matched =false;

            }

        }
        // package up the results and return them
        $ticketSummaryResults = array();
        $ticketSummaryResults["list"]=$tempTicketSum;
        $ticketSummaryResults["totalPrice"]=$totalPrice;
        $ticketSummaryResults["totalNumber"]=$totalNumber;

        return $ticketSummaryResults;
    }

    */

}