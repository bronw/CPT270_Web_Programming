<?php
require_once('resources/php_libraries/manageReservations.php');
require_once('resources/php_libraries/phpFunctions.php');


// controller for making the print page
function runPrintPage(){


//  make a print of the reservation
    if(isset($_SESSION["printingReservation"])){

        $reserveMovieID= $_SESSION["printingReservation"];

        $pageString = printTheTickets($reserveMovieID);
        clearTheRecord($reserveMovieID);


    }else{

    //  no reservation found
        $pageString =
        "<div class=\"container-fluid breadcrumbBox text-center\">".
        "<h3>Sorry no reservations ready for printing</h3>".
        "</div>";
    }

    return $pageString;
}


// make the ticket page string
function printTheTickets($reserveMovieID){

    $movieArray = getMovieArrayFromID($reserveMovieID);

    $finalStr=makePrintContainer($movieArray, $_SESSION["clientName"],
        $_SESSION["clientEmail"],
        $_SESSION["clientPhone"]);

    return $finalStr;

}


// make the movie record disappear
function clearTheRecord($reserveMovieID){


    setMovieIDStatus($reserveMovieID,"booked");
    unset($_SESSION["printingReservation"]);

}

// does all the work
function makePrintContainer($movieArray,$clientName,$clientEmail,
                            $clientPhone){


    $movie = $movieArray["movie"];
    $day = $movieArray["day"];
    $time = $movieArray["time"];


// the internal ticket list
    //  get ticket summary info to display
    $currentTicketsArray = $movieArray["tickets"];
    $priceGroup = $movieArray["priceGroup"];
    $ticketSummaryResults = getTicketsSummary2($currentTicketsArray,$priceGroup);

    //  get the parts out of the returned package
    $ticketList = $ticketSummaryResults["list"];
    $totalPrice = $ticketSummaryResults["totalPrice"];
    $totalNumber = $ticketSummaryResults["totalNumber"];



// heading
    $headingStr = makePrintHeading($clientName,$clientEmail,$clientPhone,
                                            $movie,$day,$time,$totalNumber,$totalPrice);

// tickets
    $summaryOfEachTicket= getTicketsListNamePrice($currentTicketsArray,$priceGroup);
    $ticketsStr = printTicketsItems($summaryOfEachTicket,$movie,$day,$time);


// wrapThem
    $finalStr = wrapThem($headingStr,$ticketsStr);

    return $finalStr;

}

function wrapThem($headingStr,$ticketsStr){


    $str1 = "<div class=\"container\">";

    return $str1.$headingStr.$ticketsStr."</div>";
}

// makes the list bit in the middle
function printTicketsItems($summaryOfEachTicket,$movie,$day,$time){

    $listPart = "";


    foreach($summaryOfEachTicket as $ticket){

        $listPart .="



            <!-- a single ticket-->
            <div class=\"row\">
                <div class=\"col-x-12 col-sm-3 col-lg-2\"></div>

                <div class=\"col-x-12 col-sm-6 col-lg-5\">
                    <div class=\"panel\">
                        <div class=\"row\">
                            <div class=\"col-x-12 col-sm-6 col-lg-6\">
                                <p class=\"SetLeft\"> Silverado </p>
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col-x-12 col-sm-6 col-lg-6\">
                                <p class=\"SetLeft\">".$movie."</p>
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col-x-12 col-sm-6 col-lg-6\">
                                <p class=\"SetLeft\">".$day.", ".$time."</p>
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col-x-12 col-sm-6 col-lg-6\">
                                <p class=\"SetLeft\">".$ticket["name"]."</p>
                            </div>

                            <div class=\"col-x-12 col-sm-6 col-lg-6\">
                                <p class=\"SetRight\">$ ".$ticket["cost"]."</p>
                            </div>

                        </div>

                    </div>
                </div>

                <div class=\"col-x-12 col-sm-3 col-lg-2\"></div>
            </div>



            ";
    }

    return $listPart;
}





function makePrintHeading($clientName,$clientEmail,$clientPhone,
    $movie,$day,$time,$totalNumber,$totalPrice){


    $headerStr =
        "
<!--the header ticket-->
    <div class=\"row\">
        <div class=\"col-x-12 col-sm-3 col-lg-2\"></div>


        <div class=\"col-x-12 col-sm-6 col-lg-5\">
            <div class=\"panel\">
                <div class=\"row\">
                  <div class=\"col-x-12 col-sm-6 col-lg-6\">
                      <p class=\"SetLeft\">".$clientName."</p>
                  </div>

                  <div class=\"col-x-12 col-sm-6 col-lg-6\">
                      <p class=\"SetRight\"> Silverado </p>
                  </div>
                </div>

                <div class=\"row\">
                  <div class=\"col-x-12 col-sm-6 col-lg-6\">
                      <p class=\"SetLeft\">".$clientEmail."</p>
                  </div>

                  <div class=\"col-x-12 col-sm-6 col-lg-6\">
                      <p class=\"SetRight\"> ".$movie." </p>
                  </div>
                </div>

                <div class=\"row\">
                  <div class=\"col-x-12 col-sm-6 col-lg-6\">
                      <p class=\"SetLeft\">".$clientPhone."</p>
                  </div>

                  <div class=\"col-x-12 col-sm-6 col-lg-6\">
                      <p class=\"SetRight\"> ".$day.", ".$time." </p>
                  </div>
                </div>


                <div class=\"row\">
                    <div class=\"col-x-12 col-sm-6 col-lg-6\">
                        <p class=\"SetLeft\"> COUNT </p>
                    </div>

                    <div class=\"col-x-12 col-sm-6 col-lg-6\">
                        <p class=\"SetRight\"> ".$totalNumber." </p>
                    </div>
                </div>

                <div class=\"row\">
                    <div class=\"col-x-12 col-sm-6 col-lg-6\">
                        <p class=\"SetLeft\"> TOTAL </p>
                    </div>

                    <div class=\"col-x-12 col-sm-6 col-lg-6\">
                        <p class=\"SetRight\"> $ ".$totalPrice." </p>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"col-x-12 col-sm-3 col-lg-5\"></div>
    </div>
     ";

    return $headerStr;

}