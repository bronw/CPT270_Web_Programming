<?php
/**
 * Created by PhpStorm.
 * User: Anthony
 * Date: 20/02/2016
 * Time: 9:49 AM
 *
 * May thanks to the previous student whose work
 * I emulated
 */


// increment to the next MovieID
function getNewMovieID(){
    $tempNextID = 1;
    if(isset($_SESSION["lastID"])){
        $tempNextID  +=$_SESSION["lastID"];
            //echo "print sessionID: ".$_SESSION["lastID"];
    }
    $_SESSION["lastID"]= $tempNextID;
    return $tempNextID;
}


//      returns the name of movie for time day session
function getSessionMovieName($time,$day){

    $tempGenre = getSessionGenre($time,$day);
    $tempName = getMovieName($tempGenre);


    return $tempName;
}


//      returns the genre of movie for time day session
function getSessionGenre($time,$day){
    $fileHandle = fopen("resources/data/sessionTimes.txt","r");
    $tempGenre= "";

    while(!feof($fileHandle)){
        $line = fgets($fileHandle);
        $details = explode("|",$line);


        if ((strtoupper($details[0])==strtoupper($day))&&
            (strtoupper($details[1])==strtoupper($time))){
            $tempGenre = $details[2];
            break;
        }
    }


    fclose($fileHandle);
    return $tempGenre;
}


//      returns the movie name for a genre
function getMovieName($Genre){
    $fileHandle = fopen("resources/data/genreMovies","r");
    $tempName = "";

    while(!feof($fileHandle)){
        $line = fgets($fileHandle);
        $details = explode("|",$line);

        if ($details[0]==$Genre){
            $tempName = $details[1];
            break;
        }
    }

    return $tempName;
}


//      returns the ticket prices for time day session
function getSessionTicketPrices($time,$day){

    $tempPriceGroup = getSessionPriceGroup($time,$day);
    return getTicketPriceList($tempPriceGroup);

}


//      returns the ticket price group for time day session
function getSessionPriceGroup($time,$day){
    $fileHandle = fopen("resources/data/sessionTimes.txt","r");
    $tempPriceGroup = "";

    while(!feof($fileHandle)){
        $line = fgets($fileHandle);
        $details = explode("|",$line);

        if ((strtoupper($details[0])==strtoupper($day))&&
            (strtoupper($details[1])==strtoupper($time))){
            $tempPriceGroup = $details[3];
            break;
        }
    }

    return $tempPriceGroup;
}


//      returns an array of ticket prices for a ticket price group
//      may return nothing
function getTicketPriceList($PriceGroup){

    if ($PriceGroup ==""){
        return null;
    }

    $fileHandle = fopen("resources/data/ticketPriceList.txt","r");
    $tempTickets = array();
    $tempTicketDetail = array();

    while(!feof($fileHandle)) {
        $line = fgets($fileHandle);
        $details = explode("|", $line);



        if($details[0]==trim($PriceGroup)){
            $tempTicketDetail['code'] =$details[1];
            $tempTicketDetail['type'] =$details[2];
            $tempTicketDetail['subType'] =$details[3];
            $tempTicketDetail['price'] =$details[4];

            array_push($tempTickets,$tempTicketDetail);
        }
    }

    return $tempTickets;

}


// adds the movie info into the cart array in session
function addMovieToSessionArray($movieArray){
    if(!(isset($_SESSION["cartMovies"]))){
        $_SESSION["cartMovies"]=array();
    }
    array_push($_SESSION["cartMovies"],$movieArray);
}

//  I use this in development when the page is loaded from wrong point
function addDummyMovie(){

    // set up variables to use
    $movieArray = array();
    $MovieTickets = array();
    $nextID = getNewMovieID();;

    // make array
    $movieArray["ID"] = $nextID;
    $movieArray["day"] = "FutureDay";
    $movieArray["time"] = "1AM";
    $movieArray["movie"] = "12 Monkeys";
    $movieArray["priceGroup"] = "low";
    $movieArray["status"] = "readyToBook";
    $movieArray["tickets"] = $MovieTickets;

    // add SESSION variables
    addMovieToSessionArray($movieArray);
    $_SESSION["lastID"]= $nextID;

    }


// get the array of the current movie
function getCurrentMovieArray(){

    foreach($_SESSION["cartMovies"]as $movie ){
        if ($movie["ID"]==$_SESSION["lastID"]){
            return $movie;
        }
    }
    return "";
}


// get the array for the MovieID
function getMovieArrayFromID($reserveMovieID){

    foreach($_SESSION["cartMovies"]as $movie ){
        if ($reserveMovieID==$movie["ID"]){
            return $movie;
        }
    }
    return "";
}


//  add the selected ticket into the current Movie ID
function AddTicketToMovieRecord(){


    for($t=0;$t<count($_SESSION["cartMovies"]);$t++){
        if ($_SESSION["cartMovies"][$t]["ID"]==$_SESSION["lastID"]){
            array_push($_SESSION["cartMovies"][$t]["tickets"],$_GET["ticketType"]);

        }
    }
}


// http://stackoverflow.com/questions/5139793/php-unformat-money
function dollarToInt($str){
    return preg_replace("/([^0-9\\.])/i", "", $str);
}

// returns an array of three parts
    //  list - tickets in a movieArray (
    //  totalPrice - total price tickets
    //  totalNumber - total number of tickets
function getTicketsSummary2($currentTicketsArray,$priceGroup){

    $totalPrice = 0.0;
    $totalNumber = 0;
    $ticketQuery = array();
    $tempTicketSum = array();
    $matched =false;

//  get an array of ticket types from data
    $ticketList = getTicketPriceList($priceGroup);

//  if there are no tickets
    if(!(count($currentTicketsArray)>0)){

        $ticketSummaryResults = array();
        $ticketSummaryResults["list"]=array();
        $ticketSummaryResults["totalPrice"]=0.0;
        $ticketSummaryResults["totalNumber"]=0;

    }else{

        foreach($ticketList as $ticketDATA){

            $ticketQuery["code"]=$ticketDATA["code"];
            $ticketQuery["name"]=$ticketDATA["type"]."-".$ticketDATA["subType"];
            $ticketQuery["cost"]=0.0;
            $ticketQuery["count"]=0;

            foreach($currentTicketsArray as $ticket){

                if($ticketQuery["code"]==$ticket){
                    $matched = true;
                    $ticketQuery["cost"]+= dollarToInt($ticketDATA["price"]);
                    $ticketQuery["count"]++;
                }
            }

            if ($matched){
                $totalPrice += $ticketQuery["cost"];
                $totalNumber += $ticketQuery["count"];
                array_push($tempTicketSum,$ticketQuery);
                $matched =false;

            }

        }
        // package up the results and return them
        $ticketSummaryResults = array();
        $ticketSummaryResults["list"]=$tempTicketSum;
        $ticketSummaryResults["totalPrice"]=$totalPrice;
        $ticketSummaryResults["totalNumber"]=$totalNumber;
    }


                            //    $_SESSION["totalPrice"]=$totalPrice;
                            //    $_SESSION["totalNumber"]=$totalNumber;
    return $ticketSummaryResults;





}


function getTicketsListNamePrice($currentTicketsArray,$priceGroup){


    $ticketQuery = array();
    $tempTicketSum = array();

//  get an array of ticket types from data
    $ticketList = getTicketPriceList($priceGroup);

//  if there are no tickets
    foreach($currentTicketsArray as $ticket){

        foreach($ticketList as $ticketDATA){

            if($ticketDATA["code"]==$ticket){
                $matched = true;
                $ticketQuery["cost"]= dollarToInt($ticketDATA["price"]);
                $ticketQuery["code"]=$ticketDATA["code"];
                $ticketQuery["name"]=$ticketDATA["type"]."-".$ticketDATA["subType"];
                array_push($tempTicketSum,$ticketQuery);

            }
        }

    }

    return $tempTicketSum;



}



//  removes the last entry in the movieList
function clearCurrentMovie(){


    array_pop($_SESSION["cartMovies"]);

}