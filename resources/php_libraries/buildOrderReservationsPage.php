<?php
/**
 * Created by PhpStorm.
 * User: Anthony
 * Date: 21/02/2016
 * Time: 5:29 PM
 */


//  controller for making the booking reservations page
function buildBookingContainers($clientInfoSet){


    $counter = 0;
    $pageTotal = "";


//  ensure there are some movies (testing this fails)
    if(isset($_SESSION["cartMovies"])) {
        // add the client info block
        $clientBlock = makeClientInfoFrom($clientInfoSet);

        // add block for each movie
        foreach ($_SESSION["cartMovies"] as $movieArray) {
            if ($movieArray["status"] == "readyToBook") {

                // good tickets get put into a <div>
                $counter++;
                $heading2 = "Option " . $counter;
                $pagePart = makeContainer($movieArray, $heading2);

                //  if the cart entry has not tickets then it has been disappeared
                if ($pagePart == "") {
                    $counter--;
                }

                //  holds the page text as it is built
                $pageTotal .= $pagePart;

            }
        }

        $pageTotal = $clientBlock.$pageTotal;
    }


//  the notification to client of no groups
    if($counter==0){
        $pageTotal = makeEmptyContainer();
    }

// print the page
    echo $pageTotal;

}


function makeClientInfoFrom($clientInfoSet){

    $clientName = clientText("clientName");
    $clientEmail= clientText("clientEmail");
    $clientPhone= clientText("clientPhone");


$clientInfoForm = "


<div class=\"fluidContain container-fluid col-xs-12 col-sm-12 col-md-3\">
    <h2>Your Details</h2>
    <div id=\"clientInfo\">
        <div class=\"clientDisplay\">




            <form class = \"form-inline\" role=\"form\"
                  method=\"get\"
                  action = \"orderReservation.php\">

                <div class=\"form-group\">
                    <label class=\"sr-only\" for=\"clientName\">Email address</label>
                    <input required type=\"text\" class=\"form-control\"
                           id = \"clientName\" name=\"clientName\" placeholder=\"Enter name\"
                           value = \"".$clientName."\">

                </div>

                <div class=\"form-group\">
                    <label class=\"sr-only\" for=\"clientEmail\">Email address</label>
                    <input type=\"email\" class=\"form-control\"
                           id = \"clientEmail\" name=\"clientEmail\"
                           placeholder=\"enter email\" required
                           value = \"".$clientEmail."\">
                </div>

                <div class=\"form-group\">
                    <label class=\"sr-only\" for=\"clientPhone\">Password</label>
                    <input type=\"text\" class=\"form-control\"
                           id = \"clientPhone\" name=\"clientPhone\" placeholder=\"enter phone\" required
                           value = \"".$clientPhone."\">
                </div>

                <button type=\"submit\"
                        name=\"submit\"
                        class=\"clientBtn btn btn-primary form-control\">
                    Record</button>

                ";


    // the alert if posted without client info
    $alertStr = "";
    if(!($clientInfoSet)) {

        $alertStr = "<div class= \"alert alert-danger\">
                          <strong>Hold Your Horses!</strong>
                          You have to provide your contact info before making a reservation.
                        </div>";
    }

    // finish the divs
    $finishDivs = "</form> </div> </div> </div>";


    return $clientInfoForm.$alertStr.$finishDivs;


}

function makeContainer($movieArray,$heading2){


    $movie = $movieArray["movie"];
    $day  = $movieArray["day"];
    $time = $movieArray["time"];
    $movieID = $movieArray["ID"];

//  the heading part
    $Str1= headingMiddleComponent($movie,$day,$time,$movieID);
    //  $str2= addWrapperToHeading($Str1,$heading2);  // only needed when testing with just headings




// the internal ticket list
    //  get ticket summary info to display
    $currentTicketsArray = $movieArray["tickets"];
    $priceGroup = $movieArray["priceGroup"];
    $ticketSummaryResults = getTicketsSummary2($currentTicketsArray,$priceGroup);

    //  get the parts out of the returned package
    $ticketList = $ticketSummaryResults["list"];
    $totalPrice = $ticketSummaryResults["totalPrice"];
    $totalNumber = $ticketSummaryResults["totalNumber"];

    // if the record is empty of tickets
    // trash it an empty string
    if ($totalNumber == 0){

        removeDraftReservation($movieID);
        return "";
    }


    // build the list rows
    $str3 = addTicketsIntoRowItems($ticketList);

    // add the row headers to the top
    $str3 = addHeadersAndCompleteTheTicketsList($str3);

    // add wrapperAroundList
    $str3 = addWrapperToList($str3,$movieID);

    // add the totals component
    $str3 = addTotalsDiv($str3,$totalPrice,$totalNumber);

    // wrap the header and list in a <div>
    $heading2= $heading2." id(".$movieID.")";
    $str3= wrapperHeadingToListAndTotals($Str1,$heading2,$str3);


    // get buttons
    $btnStr = addButtonsToBottom($movieID);

    // add them together
    $str3.=$btnStr;


    // final, wrap it all in a container
    $str3 = addFinalWrapperContainer($str3);


    return $str3;

}


function makeEmptyContainer(){

    // add them together
    $str2 = "<h3> There are no selections ready to reserve.</h3>";


    // final, wrap it all in a container
    $strLast = addFinalWrapperContainer($str2);
    return $strLast;


}


// the internal Movie summary bit
function headingMiddleComponent($movie,$day,$time,$movieID){


    $containerStr = "

       <div class=\"movieDisplay1\">
            <div class=\"movieDisplay\" data-toggle=\"collapse\" href=\"#selectionContainer".$movieID."\">
                    <span ID=\"movieName\">".$movie."</span>
                    <span ID=\"movieDay\">".$day."</span>
                    <span ID=\"movieTime\">".$time."</span>
                    <span class=\"delBtn popBtnDark\"><a class=\"arrow\"></a></span>
            </div>
        </div>


    ";
    return $containerStr;
}


// put a div around the outside of the header summary
function addWrapperToHeading($containerString,$heading2){


    $outer1 = "
            <h2>".$heading2."</h2>
            <div>
        " . $containerString . "
        </div>";

    return $outer1;

}

// production version of adding the H2
function wrapperHeadingToListAndTotals($Str1,$heading2,$str6){

    $wrapStr ="
        <h2>".$heading2."</h2>
        <div>
            ".$Str1."
            ".$str6."
        </div>
        ";




    return $wrapStr;
}



// put a div around the outside of the header summary
function addFinalWrapperContainer($containerString){

    $outer =
        "
        <div class=\"container col-sm-12 col-md-6\">
        "
        .$containerString.
        "</div>";

    return $outer;
}




// makes the list bit in the middle
function addTicketsIntoRowItems($ticketList){

    $listPart = "";

    if (count($ticketList)>0){
        foreach($ticketList as $ticket){

            $listPart .="

                <li class=\"row bookingRow\">
                    <span class=\"quantity\">".$ticket["count"]."</span>
                    <span class=\"TicketOrdered\">".$ticket["name"]."</span>
                    <span class=\"price\">".$ticket["cost"]."</span>
                </li>
                ";
        }
    }
    return $listPart;
}



// add list headers
function addHeadersAndCompleteTheTicketsList($str3){

    $headerStr = "
                <ul id = \"itemsToAdd\">
                    <li class=\"row columnCaptions\">
                        <span>QTY</span>
                        <span>ITEM</span>
                        <span>PRICE</span>
                    </li>
                ";

    $headerStr.=$str3;
    $headerStr.= "</ul>";

    return $headerStr;


}



// wrappers on the list
function addWrapperToList($str4,$movieID){


    $wrapperStr ="
        <!--selected Ticket Items-->
        <div id = \"selectionContainer".$movieID."\"  class=\"panel-collapse collapse\">
            <div class=\"itemsList\">
    ";

    $wrapperStr.=$str4;
    $wrapperStr.= "</div></div>";

    return $wrapperStr;
}

// the totals at the bottom
function addTotalsDiv($str5,$totalPrice,$totalNumber){

    $totalsStr =
    "
        <div class=\"itemsList\">
            <ul id = \"itemsToAdd\">
                <li class=\"row\" id=\"summed\">
                    <span class=\"quantity\">".$totalNumber."</span>
                    <span class=\"price\">".$totalPrice."</span>
                </li>
            </ul>
        </div>
    ";

    $totalsStr=$str5 . $totalsStr;

    return $totalsStr;

}



function addButtonsToBottom($movieID){

    $btnStr =
    "

    <div id = \"AlignReject\">
        <form  action=\"orderReservation.php\" method=\"get\">
        <input type=\"hidden\" name=\"dropReservation\" value=\"".$movieID."\"/>
        <input
                type=\"submit\"
                value=\"TRASH IT\"
                id=\"reject\"
                class=\"finishOut btn btn-primary btn-lg\">
        </form>
    </div>

    <div id = \"AlignOrder\">
        <form action=\"orderReservation.php\" method=\"get\">
        <input type=\"hidden\" name=\"makeReservation\" value=\"".$movieID."\"/>
        <input
                type=\"submit\"
                value=\"MAKE IT\"
                id=\"order\"
                class=\"finishGo btn btn-danger btn-lg\">
        </form>
    </div>



    ";



    return $btnStr;


}