<?php session_start();
    require_once("resources/php_libraries/phpFunctions.php");
    require_once("resources/php_libraries/buildTicketSelection.php");

    // remove all session variables
    /*session_unset();*/

// came by way of movie selection
//echo isset($_GET["day"]);

    if(isset($_GET["day"])){

        $day = $_GET["day"];
        $time = $_GET["time"];
        $movie = getSessionMovieName($time,$day);

        $priceGroup = trim(getSessionPriceGroup($time,$day));




        // set up variables to use
        $movieArray = array();
        $MovieTickets = array();
        $nextID = getNewMovieID();




        $movieArray["ID"] = $nextID;
        $movieArray["day"] = $day;
        $movieArray["time"] = $time;
        $movieArray["movie"] = $movie;
        $movieArray["priceGroup"] = $priceGroup;
        $movieArray["status"] = "readyToBook";
        $movieArray["tickets"] = $MovieTickets;
        addMovieToSessionArray($movieArray);

// adding a ticket to the current movie
    }else if(isset($_GET["ticketType"])){
        AddTicketToMovieRecord();
    }else
    {
    //  got here some way with no $_GET values
        addDummyMovie();
    }

//  get the current Movie
    $currentMovieArray = getCurrentMovieArray();
    $currentTicketsArray = $currentMovieArray["tickets"];
    $day = $currentMovieArray["day"];
    $time =$currentMovieArray["time"] ;
    $movie = $currentMovieArray["movie"];
    $status= $currentMovieArray["status"];
    $MovieID = $currentMovieArray["ID"];
    $priceGroup = trim($currentMovieArray["priceGroup"]);
    $lastID = $_SESSION["lastID"];



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>BUILD ticket selection</title>
    <?php require_once ('resources/fragments/genericHeader.php') ?>
    <link rel="stylesheet" type="text/css"
          href="resources/styles/ticketSelect.css"/>


</head>
<body>



<!--heading-->

<div class ="jumbotron">
    <h1>Add Tickets</h1>
    <p>
        Please select seats and tickets for the movie you have selected
    </p>
</div>


<?php require('resources/fragments/navBarV2.php') ?>

<!--ticket buttons-->
<?php buildHeadingContainer($movie,$day,$time);  ?>


<!--ticket buttons-->
<?php buildTicketContainer($priceGroup);  ?>

<!--ticket buttons-->
<?php buildDisplayTicketsSelected2();  ?>



